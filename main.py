import gensim.downloader as api
from gensim.models.word2vec import Word2Vec
from sklearn.cluster import KMeans
import numpy as np


# 3 classes, animals, structures, random names
LEARN_VOCAB = ['dog', 'hut', 'cat', 'house', 'Paolo']
TEST_VOCAB = ['bird', 'garage', 'Jonas']
NUM_CLUSTER = 3


def main():
    ## Get the word2vec model
    info = api.info()
    # The first run will download the word2vec model
    print("Init word2vec model")
    # Find more models on https://github.com/RaRe-Technologies/gensim-data as described in the table in the README
    model = api.load("glove-wiki-gigaword-100")


    ## Transform words to numbers
    print("Transform words into vectors")
    wordVectors = get_word2vec_repr_array(model, LEARN_VOCAB)


    ## Learn model
    print("----------------")
    print("Learn model on given vocabulary")
    kmeans = KMeans(n_clusters=NUM_CLUSTER).fit(wordVectors)

    predictedClasses = kmeans.predict(wordVectors)
    for word, predictedClass in zip(LEARN_VOCAB, predictedClasses):
        print('{} belongs to group {}'.format(word, predictedClass))


    ## Test model
    print("----------------")
    print("Test model")
    wordVectors = get_word2vec_repr_array(model, TEST_VOCAB)
    predictedClasses = kmeans.predict(wordVectors)
    for word, predictedClass in zip(TEST_VOCAB, predictedClasses):
        print('{} belongs to group {}'.format(word, predictedClass))


# Just a helper function to transform a list of words into its corresponding word2vec representation
def get_word2vec_repr_array(model, words):
    wordVectors = np.empty([len(words), model.vector_size])
    for idx,w in enumerate(words):
        wordVectors[idx,:] = get_word2vec_repr(model, w)

    return wordVectors


def get_word2vec_repr(model, word):
    if word in model.wv.vocab.keys():
        return model.wv[word]
    else:
        # Return a dummy value, zeros might not be the best choice since it is in the training set
        # TODO: Throw an error or check in advance that word does not exist in vocab, and determine class without kmeans
        return np.zeros(model.vector_size)
        # raise KeyError('"{}" is not a learned morpheme'.format(word))


if __name__ == '__main__':
    main()
